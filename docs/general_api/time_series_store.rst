Time Series Store
=================

TimeSeriesStore provides access to time series data from different backends.
All TimeSeriesStore implementations provide this methods, to see details on
a concrete Store usage go to :doc:`../stores/index`

.. autoclass:: kisters.water.time_series.core.time_series_store.TimeSeriesStore
   :members:
   :undoc-members:
   :show-inheritance:
