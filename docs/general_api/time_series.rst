Time Series
===========

TimeSeries is the object representation of the time series data it provides
all the attributes and metadata of the time series as well as access to the
data itself through the read_data_frame and write_data_frame methods.

.. autoclass:: kisters.water.time_series.core.time_series.TimeSeries
   :members:
   :undoc-members:
   :show-inheritance:
