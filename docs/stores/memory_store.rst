Memory Store
============

MemoryStore is a simple in-memory store for time series data.

.. autoclass:: kisters.water.time_series.memory.MemoryStore
   :members:
   :undoc-members:
   :show-inheritance:
