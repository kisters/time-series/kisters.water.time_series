File Store
=================

FileStore class grants access through your own time series data files.
Right now, you can have your time series data files in 3 formats, ZRXP, CSV and Pickle.

.. autoclass:: kisters.water.time_series.file_io.FileStore

.. autoclass:: kisters.water.time_series.file_io.CSVFormat

.. autoclass:: kisters.water.time_series.file_io.PickleFormat

.. autoclass:: kisters.water.time_series.file_io.ZRXPFormat
