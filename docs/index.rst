.. Kisters WATER TimeSeries documentation master file, created by
   sphinx-quickstart on Tue Mar 27 15:48:09 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

KISTERS Water Time Series API
====================================================

Welcome to the Water Time Series API documentation. This API provides access to
time series data from KiWIS and ZRXP files.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   general_api/index
   stores/index
   examples/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
