# In order to use broader scopes for asynchronous fixtures e.g. "class"
# the pytest-asyncio event_loop fixture needs to have a broader scope
# More info: https://github.com/pytest-dev/pytest-asyncio/issues/171

import pytest
from pytest_asyncio import is_async_test

from kisters.water.time_series.memory import MemoryStore
from kisters.water.time_series.parquet import ParquetStore


def pytest_collection_modifyitems(items):
    pytest_asyncio_tests = (item for item in items if is_async_test(item))
    session_scope_marker = pytest.mark.asyncio(scope="session")
    for async_test in pytest_asyncio_tests:
        async_test.add_marker(session_scope_marker, append=False)


@pytest.fixture(scope="session")
def memory_store():
    store = MemoryStore()
    yield store
    for ts in store.get_by_filter("*"):
        store.delete_time_series(path=ts.path)


@pytest.fixture(scope="class")
def memory_store_cls(request, memory_store):
    request.cls.STORE = memory_store


@pytest.fixture(scope="session")
def parquet_store():
    store = ParquetStore("test.pq")
    yield store
    store._client._filename.unlink(missing_ok=True)


@pytest.fixture(scope="class")
def parquet_store_cls(request, parquet_store):
    request.cls.STORE = parquet_store
