# KISTERS Python Time Series API

This is the KISTERS Python Time Series API, 
a common client API for all time series backends at KISTERS.

## Installation

To install, simply use pip:

```bash
pip install kisters.water.time_series
```

# Documentation

Documentation is available at https://kisterswatertime-series.readthedocs.io/en/latest/ .